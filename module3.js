"use strict";

const { name } = require('./module1');

global.HELLO = "Hello from module3";
global.name = name;
console.log(`Module 3! Name from module 1 :  ${name} `);