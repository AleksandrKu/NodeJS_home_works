"use strict";
const obj = {
    name: "Aleksander",
    age: 40
};

obj.str =  "Hello";
obj.function = (number = 10) => obj.age + number;
module.exports = obj;