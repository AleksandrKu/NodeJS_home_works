"use strict";

console.error("From Module2: ", HELLO);

exports.sum = "sum";
exports.add = (val1, val2) =>(val1 < 0 || val2 < 0 ) ? "Unallowed argument" : val1 + val2;
