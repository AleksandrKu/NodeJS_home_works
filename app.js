"use strict";
require('./module3');
const obj1 = require('./module1');
const {add} = require('./module2');
const obj4 = require('./module4');

const chalk = require('chalk');
const log = console.log;
const error = chalk.bold.red;

const argv = require('minimist')(process.argv.slice(2));
const boxen = require('boxen');
const Conf = require('conf');
const cliTruncate = require('cli-truncate');
const firstRun = require('first-run');
const Table = require('cli-table');
const CFonts = require('cfonts');

CFonts.say('Hi in my App', {
    font: 'block',              //define the font face
    align: 'left',              //define text alignment
    colors: ['system'],         //define all colors
    background: 'transparent',  //define the background color, you can also use `backgroundColor` here as key
    letterSpacing: 1,           //define letter spacing
    lineHeight: 1,              //define the line height
    space: true,                //define if the output text should have empty lines on top and on the bottom
    maxLength: '0',             //define how many character can be on one line
});

log(obj1);
console.log(obj1.function(5));
log(error(add(-1, 2)));
console.log(add(8, 2));
console.log(obj4);
log(chalk.blue("Глобальное имя " + name));

console.log("Path: " + chalk.gray(process.argv[1]));
const config = new Conf();
config.set('configs', chalk.yellow('ENV.port'));
console.log("PORT : " + config.get('configs'));
console.log("NODE_ENV : " + chalk.yellow(process.env.NODE_ENV));
if (argv) {
    let table = new Table({
        head: [chalk.red('key'), chalk.blue('value')]
    });
    var config_array = [];
    for (let key in argv) {
        config_array.push(argv[key]);
        table.push([chalk.red(key), chalk.blue(argv[key])]);
    }
    console.log(table.toString());
}
if (firstRun()) {
    log(boxen(chalk.green("First time run program"), {padding: 1}));
}
else {
    log(boxen(chalk.red("Second time run program. Cleaning firstRun()"), {padding: 1}));
    firstRun.clear();
}

//console.log(boxen(cliTruncate("The end of my program ", 8), {padding: 1, margin: 0, borderStyle: 'round'}));

